# PDG19 - TravelBook

## Description

TravelBook is a social web application for people who want to keep track of their holiday destinations, illustrate them with their favourite pictures and share them with their friends and with the world.

The website allows users to follow their friends, look at their travels, read the descriptions and see the pictures. They can add their own experiences to the map, by entering their destinations' addresses, adding images and a description. The locations they visited will appear on the map. In case they visited several consecutive locations, the users can visualize their travel on the map.

## Using TravelBook

To use TravelBook, you just have to [access the website](https://travelbook.beer) and create an account.

Visit our [landing page](https://heig-boom.gitlab.io/travelbook) to get a high level view of the application.

## Navigate the specification

### Choices

- The project's **minimum viable product** is described in details in [this file](specification/wiki/MVP.md).

- The **scenario** for the final demo of the project can be found in [this file](specification/wiki/scenario.md).

- For a **long term vision** of the project, see the landing page at [this address](https://heig-boom.gitlab.io/travelbook).

- The **architecture choices** are documented [here](specification/wiki/architectureChoices.md).

### Frontend

- The web app's functional **mockup** can be found at [this address](https://xd.adobe.com/view/1c3f3c18-60ba-4089-772c-b9d90b0607cb-6a0e/?fullscreen) and in [this folder](specification/UX_UI).

### Backend

- Our database **relational model** can be found [here](specification/database/model.png).

- A simple, readable definition of the **REST API endpoints** can be found [here](specification/database/RESTEndpoints.md).

### DevOps

- A description of the pipeline and the jobs it is comprised of can be found [here](specification/wiki/PipelineConfig.md).

- A quick description of the infrastructure used to test and deploy the app can be found in [this file](specification/wiki/Infrastructure.md).

---

## For developers

Access the test server at [test.travelbook.beer](https://test.travelbook.beer). (The database is not persistent. Strictly use for testing purposes!)

### Project architecture

We're exposing a REST API using a _Django (2.2.6)_ backend with a _PostgreSQL_ database (manipulated through the Django ORM).

On the frontend, _Vue.js_ builds the interfaces.

(That should have scared away any non-programmer. Now we can talk.)

### Setup for development

- Make sure you have `git` installed, as well as `python` **3** and `pip`.
- Clone the repo on your local machine.

#### Using Docker

- Make sure you have `docker` and `docker-compose` installed.
- Position yourself in the `docker/development` folder.

```
# Build the different images and launch the containers
# Add -d flag to launch them in the background
docker-compose up --build

# Create a super user for the backend's administration site
docker-compose exec backend python manage.py createsuperuser
```

- You can now access the [frontend](http://localhost),
- the [api interface](http://localhost/api),
- the [admin panel](http://localhost/api/admin) with the credentials you set earlier,
- and the [traefik dashboard](http://localhost:8080).

If you change the **models** in a way or the other, you will have to run the migrations before you see the changes. To do this, use the following commands.

```
# Log into your backend container
docker exec -it travelbook-backend bash

# Create the migration files
python manage.py makemigrations

# Run the backend migrations
python manage.py migrate
```

#### Backend without docker

- Create a python virtualenv in the `src/backend` folder and activate it:

```
python -m venv venv
source venv/bin/activate
```

- Install requirements (Django and such):

```
python -m pip install -r requirements.txt
```

- Make sure to source the environment variables to match your current configuration:
    - You can use the `docker/development/env_file_dev` file as a `.env` file to source vars.
    - If you don't default values will be set, which might not fit your setup.
- You can now open the `src/backend` folder in your favourite editor and work on the [backend](http://localhost:8000).

### Merge requests

Any merge request must be approved by at least one _other_ team member.

We considered using a `CODEOWNERS` file and separating responsibilities in the team, but since we are such a small team, we decided it would be a hassle more than a help.

### CI / CD

- A commit in any branch will trigger a job to run the Django **tests** in the backend and one for the frontend tests. Tests are run on our private GitLab Runner, by just installing dependencies and running the test scripts.

- For commits to master, and if the tests passed, we go on to the **staging** job. This sets up the environment variable on the remote test server. Then, it updates the repository on the remote and runs the docker images in `docker/staging/docker-compose.yml`.

- A manual trigger then appears on the pipeline page on our Gitlab, so that a developer can trigger a job to deploy the app to **production** when it has been tested sufficiently in the staging environment.

- A simple pipeline job is set up so that any commit to _master_ that contains changes to the files in the `landing-page` folder updates the project's [landing page](https://heig-boom.gitlab.io/travelbook), hosted on **GitLab Pages**. To do so, it simply updates the files in the public folder of the repository.

For details concerning the pipeline and the remote servers used, see [this file](specification/wiki/PipelineConfig.md) and [this one](specification/wiki/Infrastructure.md).

## Mockup main page

![Main page mockup](specification/UX_UI/mainpage.png)
