echo "----- Deploying -----"

# Check for needed software
echo "----- Download openssh -----"
apk add openssh

# SSH config
echo "----- SSH Config -----"
mkdir -p ~/.ssh
chmod 700 ~/.ssh

eval "$(ssh-agent -s)"
echo "$STAGING_SERVER_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

ssh-keyscan -H $STAGING_HOSTNAME >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# Commands in remote host
echo "----- Setup vars, run updated containers -----"
ssh -T $STAGING_SERVER_USER@$STAGING_HOSTNAME << EOF
    cd /travelbook/docker/staging

    # Down server
    docker-compose down -v
    rm env_file_test

    # Update sources
    git pull origin master

    # Setup env vars for docker-compose file
    export TB_DB_PASSWORD=$STAGING_DB_PASSWORD
    echo "TB_SECRET_KEY=$STAGING_SECRET_KEY" > env_file_test
    echo "TB_DEBUG=$STAGING_DEBUG" >> env_file_test
    echo "TB_DB_HOST=$TB_DB_HOST" >> env_file_test
    echo "TB_DB_USER=$TB_DB_USER" >> env_file_test
    echo "TB_DB_PASSWORD=$STAGING_DB_PASSWORD" >> env_file_test
    echo "TB_SERVER_IP=$STAGING_HOSTNAME" >> env_file_test

    # Launch containers
    docker-compose up --build -d
EOF
