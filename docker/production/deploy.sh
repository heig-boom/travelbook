echo "----- Deploying -----"

# Check for needed software
echo "----- Download openssh -----"
apk add openssh

# SSH config
echo "----- SSH Config -----"
mkdir -p ~/.ssh
chmod 700 ~/.ssh

eval "$(ssh-agent -s)"
echo "$PROD_SERVER_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

ssh-keyscan -H $PROD_HOSTNAME >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# Commands in remote host
echo "----- Setup vars, run updated containers -----"
ssh -T $PROD_SERVER_USER@$PROD_HOSTNAME << EOF
    cd /travelbook/docker/production

    # Update sources
    rm env_file
    git pull origin master

    # Setup env vars for docker-compose file
    export TB_DB_PASSWORD=$PROD_DB_PASSWORD
    echo "TB_SECRET_KEY=$PROD_SECRET_KEY" > env_file
    echo "TB_DEBUG=$PROD_DEBUG" >> env_file
    echo "TB_DB_HOST=$TB_DB_HOST" >> env_file
    echo "TB_DB_USER=$TB_DB_USER" >> env_file
    echo "TB_DB_PASSWORD=$PROD_DB_PASSWORD" >> env_file
    echo "TB_SERVER_IP=$PROD_HOSTNAME" >> env_file

    # Update containers with minimal downtime
    docker-compose down
	docker-compose up --build -d
EOF
