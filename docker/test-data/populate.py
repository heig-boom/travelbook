from random import randrange
from json import dump

from faker import Faker
from pytz import UTC

"""Generate test data for the Travelbook backend

Generate json fixtures from random data for every table in the database.

Traveluser, Subscribe, Travel, Like, Step, Image
"""

faker = Faker()


basepath = "../../src/backend/travelapi/fixtures/"

nbr_of_users = 100
nbr_of_subs = 1000
nbr_of_travels = 100
nbr_of_likes = 1000
nbr_of_steps = 1000
nbr_of_images = 5000


usernames = []

for i in range(1, nbr_of_users):
    username = faker.first_name()

    if username in usernames:
        usernames.append(username + str(i))
    else:
        usernames.append(username)


# =========================================================
# User data
users = []

for i in range(1, nbr_of_users):
    entry = {}

    entry['model'] = "travelapi.traveluser"

    entry['pk'] = i

    entry['fields'] = {
        'email': faker.email(),
        'first_name': faker.first_name(),
        'last_name': faker.last_name(),
        'username': usernames[i-1],
        'profile_picture': "gallery/"+str(randrange(200, 300))+".jpg",
        'password': faker.last_name(),
        'is_active': "True",
        'is_admin': "False"
    }

    users.append(entry)

with open(basepath + '1_traveluser.json', 'w') as outfile:
    dump(users, outfile, indent=2)


# =========================================================
# Subscription data
subs = []

for i in range(1, nbr_of_subs):
    entry = {}

    entry['model'] = "travelapi.subscribe"

    entry['pk'] = i

    entry['fields'] = {
        'subscriber': randrange(1, nbr_of_users),
        'subscribed': randrange(1, nbr_of_users)
    }

    subs.append(entry)

with open(basepath + '2_subscribe.json', 'w') as outfile:
    dump(subs, outfile, indent=2)


# =========================================================
# Travel data
travels = []

for i in range(1, nbr_of_travels):
    entry = {}

    entry['model'] = "travelapi.travel"

    entry['pk'] = i

    entry['fields'] = {
        'title': faker.sentence(nb_words=5, variable_nb_words=True),
        'banner': "gallery/"+str(randrange(200, 300))+".jpg",
        'user': randrange(1, nbr_of_users)
    }

    travels.append(entry)

with open(basepath + '3_travel.json', 'w') as outfile:
    dump(travels, outfile, indent=2)


# =========================================================
# Likes
likes = []

for i in range(1, nbr_of_likes):
    entry = {}

    entry['model'] = "travelapi.like"

    entry['pk'] = i

    entry['fields'] = {
        'user': randrange(1, nbr_of_users),
        'travel': randrange(1, nbr_of_travels)
    }

    likes.append(entry)

with open(basepath + '4_like.json', 'w') as outfile:
    dump(likes, outfile, indent=2)


# =========================================================
# Step data
steps = []

for i in range(1, nbr_of_steps):
    entry = {}

    entry['model'] = "travelapi.step"

    entry['pk'] = i

    entry['fields'] = {
        'travel': randrange(1, nbr_of_travels),
        'title': faker.sentence(nb_words=5, variable_nb_words=True),
        'description': faker.paragraph(nb_sentences=6, variable_nb_sentences=True),
        'start_date': str(faker.date_time(tzinfo=UTC, end_datetime=None)),
        'end_date': str(faker.date_time(tzinfo=UTC, end_datetime=None)),
        'city': faker.city(),
        'street': faker.street_name(),
        'streetNum': randrange(1, 60),
        'postal_code': faker.postalcode(),
        'longitude': float(faker.longitude()),
        'latitude': float(faker.latitude())
    }

    steps.append(entry)

with open(basepath + '5_step.json', 'w') as outfile:
    dump(steps, outfile, indent=2)


# =========================================================
# Images
images = []

for i in range(1, nbr_of_images):
    entry = {}

    entry['model'] = "travelapi.image"

    entry['pk'] = i

    entry['fields'] = {
        'url': "gallery/"+str(randrange(200, 300))+".jpg",
        'step': randrange(1, nbr_of_steps),
        'travel': randrange(1, nbr_of_travels)
    }

    images.append(entry)

with open(basepath + '6_image.json', 'w') as outfile:
    dump(images, outfile, indent=2)
