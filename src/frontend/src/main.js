import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import BootstrapVue from 'bootstrap-vue'
import APIService from './api/service'
import store from './store';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.config.productionTip = false

// We use the api service to communicate with travelbook api
export let apiService = new APIService();

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
