import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: "/",
      component: () => import("@/views/Home")
    },
    {
      name: "travel",
      path: "/travel/:id",
      component: () => import("@/views/Travel"),
      props: true
    },
    {
      name: "travel-new",
      path: "/trip/new",
      component: () => import("@/views/TravelNew"),
      props: true
    },
    {
      name: "profile",
      path: "/profile",
      component: () => import("@/views/Profile"),
      props: true
    },
  ]
});