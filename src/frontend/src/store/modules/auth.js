/* eslint-disable no-console */
import axios from 'axios'
import jwt_decode from 'jwt-decode'

import { OBTAIN_TOKEN, REFRESH_TOKEN, INSPECT_TOKEN } from "../actions";
import { SET_TOKENS, UPDATE_TOKEN, REMOVE_TOKEN, SET_ERROR } from "../mutations";

const state = {
  errors: null,
  accessToken: localStorage.getItem("accessToken"),
  refreshToken: localStorage.getItem("refreshToken"),
  endpoints: {
    obtainJWT: "/session",
    refreshJWT: "/session/refresh"
  }
};

const actions = {
  [OBTAIN_TOKEN](context, credentials) {
    const payload = {
      username: credentials.username,
      password: credentials.password
    };

    return new Promise(resolve => {
      axios.post(state.endpoints.obtainJWT, payload)
        .then((response) => {
          // If the request succeeded, store tokens and resolve
          context.commit(SET_TOKENS, response.data);
          resolve(response);
          // reload whole page to change header
          location.reload()
        })
        .catch(({ response }) => {
          // In case we don't get a 200, store errors
          context.commit(SET_ERROR, response.data);
        });
    });
  },
  [REFRESH_TOKEN](context) {
    const payload = {
      refresh: state.refreshToken
    };

    axios.post(state.endpoints.refreshJWT, payload)
      .then(response => {
        context.commit(UPDATE_TOKEN, response.data.access);
      })
      .catch(({ response }) => {
        console.log(response.data);
      });
  },
  [INSPECT_TOKEN](context) {
    const token = state.accessToken;
    if (token) {
      const decoded = jwt_decode(token);
      const exp = decoded.exp;
      if (exp < Date.now() / 1000) {
        context.dispatch(REFRESH_TOKEN);
      } else if (exp - (Date.now() / 1000) < 300) {
        // DO NOTHING, DO NOT REFRESH          
      } else {
        // TODO: PROMPT USER TO RE-LOGIN, THIS ELSE CLAUSE COVERS THE CONDITION WHERE A TOKEN IS EXPIRED AS WELL
      }
    }
  }
};

// Set all mutations that will actually change state in store
const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [UPDATE_TOKEN](state, newToken) {
    localStorage.setItem("accessToken", newToken);
    state.accessToken = newToken;
  },
  [SET_TOKENS](state, tokens) {
    localStorage.setItem("accessToken", tokens.access);
    localStorage.setItem("refreshToken", tokens.refresh);
    state.accessToken = tokens.access;
    state.refreshToken = tokens.refresh;
  },
  [REMOVE_TOKEN](state) {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("refreshToken");
    state.accessToken = null;
    state.refreshToken = null;
  },
};

export default {
  state,
  actions,
  mutations,
};
