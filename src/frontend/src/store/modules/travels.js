import { FETCH_TRAVELS, FETCH_TRAVELS_FILTERS } from "../actions";
import { FETCH_START, FETCH_END } from "../mutations";
import TravelsService from "../../api/modules/travels";
import UserService from "../../api/modules/user";

// Initial state
const state = {
  travelCount: null,
  travels: [],
  isLoading: true,
};

// Set computed properties for stores
const getters = {
  travels: state => {
    return state.travels;
  },
  isLoading: () => {
    return state.isLoading;
  }
};

// Set all actions that will commit mutations
const actions = {
  [FETCH_TRAVELS]({ commit }) {
    commit(FETCH_START);
    const ts = new TravelsService();
    const us = new UserService();

    ts.get()
      .then(({ data }) => {
        for (let travel of data.results) {
          // Let's replace user id by the username
          us.get(travel.user)
            .then(({ data }) => {
              travel.user = data.username;
            })
            .catch(error => {
              throw new Error(error);
            });
        }
        return data;
      })
      .then((data) => {
        commit(FETCH_END, data);
      })
      .catch(error => {
        throw new Error(error);
      });
  },
  [FETCH_TRAVELS_FILTERS](context, filters) {
    context.commit(FETCH_START);
    const ts = new TravelsService();
    const us = new UserService();

    ts.getWithFilters(filters)
      .then(({ data }) => {
        for (let travel of data.results) {
          // Let's replace user id by the username
          us.get(travel.user)
            .then(({ data }) => {
              travel.user = data.username;
            })
            .catch(error => {
              throw new Error(error);
            });
        }
        return data;
      })
      .then((data) => {
        context.commit(FETCH_END, data);
      })
      .catch(error => {
        throw new Error(error);
      });
  }
};

// Set all mutations that will actually change state in store
const mutations = {
  [FETCH_START](state) {
    state.isLoading = true;
  },
  [FETCH_END](state, data) {
    state.travels = data.results;
    state.travelCount = data.count;
    state.isLoading = false;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
}
