import { FETCH_USER, CREATE_USER, UPDATE_USER, UPDATE_PASSWORD, FETCH_SUBSCRIBERS, FETCH_SUBSCRIBED, INSPECT_TOKEN } from "../actions"
import { SET_USER, SET_ERROR, SET_SUBSCRIBERS, SET_SUBSCRIBED } from "../mutations"
import UserService from "../../api/modules/user"

// Intial state
const state = {
  errors: null,
  user: {
    id: null,
    email: "",
    username: "",
    lastName: "",
    firstName: "",
  },
  subscribers: {
    ids: []
  },
  subscribed: {
    ids: []
  }
};

// Set computed properties for stores
const getters = {
  user: state => {
    return state.user;
  }
};

// Set all actions that will commit mutations
const actions = {
  async [FETCH_USER]({ commit }, id) {
    return new UserService().get(id)
      .then(({ data }) => {
        commit(SET_USER, data);
      })
      .catch(error => {
        throw new Error(error);
      });
  },
  [CREATE_USER](context, credentials) {
    const payload = {
      last_name: credentials.lastname,
      first_name: credentials.firstname,
      username: credentials.username,
      email: credentials.email,
      password: credentials.password
    };

    // Send payload to register endpoint
    return new Promise(resolve => {
      new UserService().post(payload)
        .then((response) => {
          // Remove error and resolve promise
          context.commit(SET_ERROR, null);
          resolve(response);
        })
        .catch(({ response }) => {
          // Set errors to display on form
          context.commit(SET_ERROR, response.data);
        });
    });
  },
  [UPDATE_USER](context, credentials) {
    const payload = {
      last_name: credentials.lastName,
      first_name: credentials.firstName,
      username: credentials.username,
      email: credentials.email,
      password: credentials.password
    };

    // Get the token and refresh if needed
    context.dispatch(INSPECT_TOKEN);

    // Send payload to travelusers/id endpoint
    new UserService().patch(credentials.id, payload, localStorage.getItem('accessToken'))
      .then(() => {
        // Remove error and resolve promise
        context.commit(SET_ERROR, null);
      })
      .catch(({ response }) => {
        // Set errors to display on form
        context.commit(SET_ERROR, response.data);
      });
  },
  [UPDATE_PASSWORD](context, payload) {
    // Send payload to travelusers/id endpoint
    new UserService().patch(payload.id, { password: payload.password }, localStorage.getItem('accessToken'))
      .then(() => {
        // Remove error and resolve promise
        context.commit(SET_ERROR, null);
      })
      .catch(({ response }) => {
        // Set errors to display on form
        context.commit(SET_ERROR, response.data);
      });
  },
  async [FETCH_SUBSCRIBERS](context, id) {
    return new UserService().getSubscribers(id)
      .then(({ data }) => {
        context.commit(SET_SUBSCRIBERS, data);
      })
      .catch(error => {
        throw new Error(error);
      });
  },
  async [FETCH_SUBSCRIBED](context, id) {
    return new UserService().getSubscribed(id)
      .then(({ data }) => {
        context.commit(SET_SUBSCRIBED, data);
      })
      .catch(error => {
        throw new Error(error);
      });
  }
};

// Set all mutations that will actually change state in store
const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_USER](state, user) {
    state.user.id = user.id;
    state.user.email = user.email;
    state.user.username = user.username;
    state.user.lastName = user.last_name;
    state.user.firstName = user.first_name;
  },
  [SET_SUBSCRIBERS](state, subscribers) {
    subscribers.forEach(sub => {
      state.subscribed.ids.push(sub.subscribed);
    });
  },
  [SET_SUBSCRIBED](state, subscribed) {
    subscribed.forEach(sub => {
      state.subscribers.ids.push(sub.subscriber);
    });
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}