import { FETCH_TRAVEL, CREATE_TRAVEL, INSPECT_TOKEN } from "../actions";
import TravelsService from "../../api/modules/travels";
import jwt_decode from 'jwt-decode';
import { SET_TRAVEL, SET_ERROR, SET_TRAVEL_BASIC_INFOS } from "../mutations";

// Initial state
const state = {
  errors: null,
  travel: {
    name: null,
    user: null,
    banner: null,
    step: {
      title: null,
      description: null,
      startDate: null,
      endDate: null,
      street: null,
      streetNum: null,
      zip: null,
      city: null,
      gallery: null,
    },
  },
};

// Set computed properties for stores
const getters = {
  banner: state => {
    return state.banner;
  },
  travel: state => {
    return state.travel;
  },
  errors: state => {
    return state.errors;
  }
};

// Set all actions that will commit mutations
const actions = {
  [FETCH_TRAVEL]({ commit }, id) {
    const ts = new TravelsService();
    ts.getTravel(id)
    .then(({ data }) => {
      commit(SET_TRAVEL_BASIC_INFOS, data);
    })
    .catch(error => {
      throw new Error(error);
    })
    return ts.getTravelStep(id)
      .then(({ data }) => {
        commit(SET_TRAVEL, data);
      })
      .catch(error => {
        throw new Error(error);
      });
  },
  [CREATE_TRAVEL]({ dispatch, commit,  state }) {
    // Get the token and refresh if needed
    dispatch(INSPECT_TOKEN);
    const decoded = jwt_decode(localStorage.getItem("accessToken"));

    // Create the travel's formData to send to the backend
    let formData = new FormData();
    formData.append("title", state.travel.name);
    formData.append("user", decoded.user_id);
    if (state.travel.banner) {
      formData.append("banner", state.travel.banner, "banner.png");
    }

    const ts = new TravelsService();

    return ts.createTravel(formData, localStorage.getItem("accessToken"))
      .then(({ data }) => {
        if (data.detail === "success") {
          // Get id of the created travl
          const travelId = data.id;

          // Create the step's formData to send to the backend
          let formData = new FormData();
          const startDate = new Date(state.travel.step.startDate);
          const endDate = new Date(state.travel.step.endDate);
          formData.append("title", state.travel.step.title);
          formData.append("description", state.travel.step.description);
          formData.append("start_date", startDate.toISOString());
          formData.append("end_date", endDate.toISOString());
          formData.append("street", state.travel.step.street);
          formData.append("streetNum", state.travel.step.streetNum);
          formData.append("postal_code", state.travel.step.zip);
          formData.append("city", state.travel.step.city);

          if (state.travel.step.gallery) {
            for (let image of state.travel.step.gallery) {
              formData.append("images", image);
            }
          }

          ts.createStep(formData, travelId, localStorage.getItem("accessToken"))
            .then(({ data }) => {
              // Remove error and resolve promise
              commit(SET_ERROR, null);
              if (data.detail === "success") {
                location.href = "/";
              }
            })
            .catch(({ response }) => {
              // Set errors to display on form
              commit(SET_ERROR, response.data);
            });
        }
      })
      .catch(({ response }) => {
        commit(SET_ERROR, response.data);
      })
  }
}

// Set all mutations that will actually change state in store
const mutations = {
  [SET_TRAVEL_BASIC_INFOS](state, travel) {
    state.travel.banner = travel.banner;
  },
  [SET_TRAVEL](state, travel) {
    const banner = state.travel.banner;
    state.travel = travel.results[0];
    state.travel.banner = banner;
  },
  [SET_ERROR](state, error) {
    state.errors = error;
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}