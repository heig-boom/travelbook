import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import auth from './modules/auth';
import travels from './modules/travels';
import travel from './modules/travel';
import user from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    travels,
    travel,
    user
  }
});
