import { apiService } from '../../main'

/**
 * Class representing abstractions for making API requests.
 * In this case, we only deal with users
 */
class UserService {
  /**
   * Gets the user with the specified id
   */
  get(id) {
    return apiService.get('travelusers', id);
  }

  /**
   * Gets the subscribers of the user with the specified id
   */
  getSubscribers(id) {
    return apiService.get(`travelusers/${id}/subscribed`);
  }

  /**
   * Gets the users the user with the specified id subscribed to
   */
  getSubscribed(id) {
    return apiService.get(`travelusers/${id}/subscribers`);
  }

  /**
   * Posts a new user identified by the body
   */
  post(body) {
    return apiService.post('travelusers', body);
  }

  /**
   * Puts a user identified by the body
   */
  patch(id, body, accessToken) {
    let config = {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    };
    return apiService.patch(`travelusers/${id}`, body, config);
  }
}

export default UserService;