import { apiService } from '../../main'

/**
 * Class representing abstractions for making API requests.
 * In this case, we only deal with travels
 */
class TravelsService {
  /**
   * Gets all travels
   */
  get() {
    return apiService.get('travels');
  }

  /**
   * Gets all travels that meet certain filters
   * The filters parameter must be an object containing valid querystring params
   */
  getWithFilters(filters) {
    if (filters === undefined) {
      return this.get();
    }

    let querystring = '';

    for (let [key, filter] of Object.entries(filters)) {
      querystring += `${key}=${filter}&`;
    }

    return apiService.get(`travels?${querystring}`);
  }

  /**
   * Gets the travel basic infos with the specified id
   * @param {*} id The id of the travel to get
   */
  getTravel(id) {
    const url = `travels/${id}`;
    return apiService.get(url);
  }

  /**
   * Gets the travel steps with the specified id
   * @param {*} id The id of the travel to get
   */
  getTravelStep(id) {
    const url = `travels/${id}/steps`;
    return apiService.get(url);
  }

  /**
   * Creates a new travel
   * @param {*} formData formData to send to the backend
   * @param {*} decoded jwt token
   */
  createTravel(formData, accessToken) {
    let config = {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    };
    return apiService.postWithConfig('travels', formData, config);
  }

  /**
   * Creates a new step for a given travel
   * @param {*} formData formData to send to the backend
   * @param {*} travelId The given travel id
   * @param {*} accessToken jwt token
   */
  createStep(formData, travelId, accessToken) {
    let config = {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    };
    const url = `travels/${travelId}/steps`
    return apiService.postWithConfig(url, formData, config);
  }
}

export default TravelsService;