/**
 * @constant {string}
 * @default 
 */
export const API_URL = "/api/v1";
export default API_URL;
