import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { API_URL } from '@/api/config';

/**
 * Class representing abstractions for making API requests
 */
class APIService {
  /**
   * Creates and inits the api service
   */
  constructor() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = API_URL;
  }

  /**
   * Makes a query to the travelbook API
   * @param {*} resource The resource we want to get from API
   * @param {*} params Parameters to send to the API endpoint
   */
  query(resource, params) {
    return Vue.axios.get(resource, params).catch(error => {
      throw new Error(`APIService - ${error}`)
    })
  }

  /**
   * Makes GET request to the travelbook API
   * @param {*} resource The resource from which we want to get data
   * @param {*} slug URL slug used to identify the resource
   */
  get(resource, slug = "") {
    let url = slug ? `${resource}/${slug}` : resource;
    return Vue.axios.get(url).catch(error => {
      throw new Error(`APIService - ${error}`);
    });
  }

  /**
   * Makes POST request to the travelbook API
   * @param {*} resource The resource from which we want to post data
   * @param {*} params Parameters to send to the API endpoint
   */
  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  }

  /**
   * Makes POST request to the travelbook API
   * @param {*} resource The resource from which we want to post data
   * @param {*} params Parameters to send to the API endpoint
   *@param {*} params Axios config for the single request
   */
  postWithConfig(resource, params, config) {
    return Vue.axios.post(`${resource}`, params, config);
  }

  /**
   * Makes UPDATE request to the travelbook API
   * @param {*} resource The resource from which we want to update data
   * @param {*} slug URL slug used to identify the resource
   * @param {*} params Parameters to send to the API endpoitn
   */
  update (resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  }

  /**
   * Makes PUT request to the travelbook api
   * @param {*} resource The resource from which we want to put data
   * @param {*} params Parameters to send to the API endpoint
   */
  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  }

  /**
   * Makes PATCH request to the travelbook api
   * @param {*} resource The resource from which we want to put data
   * @param {*} params Parameters to send to the API endpoint
   */
  patch(resource, params, config) {
    return Vue.axios.patch(`${resource}`, params, config);
  }
  
  /**
   * Makes DELETE request to the travelbook API
   * @param {*} resource The resource from which we want to delete data
   */
  delete(resource) {
    return Vue.axios.delete(resource).catch(error => {
      throw new Error(`APIService - ${error}`);
    });
  }
}

export default APIService;