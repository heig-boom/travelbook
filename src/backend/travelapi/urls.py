"""Travelapi app URL Configuration

Contains patterns for api endpoints and JWT token (session)
"""
from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from django.views.static import serve
from django.conf import settings
from travelapi.views import travels_endpoint, travelusers_endpoint, steps_endpoint, subscribe_endpoint, likes_endpoint

urlpatterns = [
    # Travels related endpoints
    path('travels', travels_endpoint.AllTravels.as_view(), name='travels'),
    path('travels/<int:id>', travels_endpoint.OneTravel.as_view(), name='travel'),
    path('travels/<int:id>/steps', steps_endpoint.AllSteps.as_view(), name='steps'),
    path('travels/<int:id_travel>/steps/<int:id_step>', steps_endpoint.OneStep.as_view(), name='step'),
    path('media/<path:url>', serve, {'document_root': settings.MEDIA_ROOT}),

    # Users related endpoints
    path('travelusers/<int:id>', travelusers_endpoint.OneTraveluser.as_view(), name='traveluser'),
    path('travelusers', travelusers_endpoint.Travelusers.as_view(), name='register'),

    # Likes and subscriptions
    path('travels/<int:id>/likes', likes_endpoint.Likes.as_view(), name='likes'),
    path('travelusers/<int:id>/subscribed', subscribe_endpoint.AllSubscribed.as_view(), name='subscribed'),
    path('travelusers/<int:id>/subscribers', subscribe_endpoint.AllSubscribers.as_view(), name='subscribers'),
    path('travelusers/subscription', subscribe_endpoint.AllSubscribed.as_view(), name='subscribe'),

    # Get and refresh JWT token
    path('session', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('session/refresh', TokenRefreshView.as_view(), name='token_refresh'),
]
