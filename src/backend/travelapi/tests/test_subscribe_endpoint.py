from django.urls import reverse
from rest_framework import status

from travelapi.models import Subscribe
from travelapi.serializers.subscribe_ser import SubscribeSerializer
from travelapi.tests.setup_tests import SetUpForAllEndpointTests


class GetAllSubscribeOfOneTraveluserTest(SetUpForAllEndpointTests):
    """Test module for GET all subscribe of a given traveluser"""

    def test_get_all_subscribed(self):
        """Create some subscribe, then tests it"""
        # get API response
        response = self.client.get(reverse('subscribed', kwargs={'id': self.amandine.pk}))
        # get data from db
        subscribe = Subscribe.objects.filter(subscriber=self.amandine.pk)
        serializer = SubscribeSerializer(subscribe, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_subscriber(self):
        """Create some subscribe, then tests it"""
        # get API response
        response = self.client.get(reverse('subscribers', kwargs={'id': self.charlene.pk}))
        # get data from db
        subscribe = Subscribe.objects.filter(subscribed=self.charlene)
        serializer = SubscribeSerializer(subscribe, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CreateNewSubscriptionTest(SetUpForAllEndpointTests):
    """Test module for subscribing to a user"""

    def test_create_subscription_without_auth(self):
        """Tests for authorization when posting a new subscription"""
        response = self.client.post(
            reverse('subscribe'),
            {'subscriber': self.amandine.pk, 'subscribed': self.archie.pk},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_valid_subscription(self):
        """Tests to POST a new subscription"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.amandine_token)

        response = self.client.post(
            reverse('subscribe'),
            {'subscriber': self.amandine.pk, 'subscribed': self.archie.pk},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_subscription(self):
        """Tests to POST a new invalid subscription"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.amandine_token)

        response = self.client.post(
            reverse('subscribe'),
            {'subscribed': self.archie.pk},  # Missing subscriber field
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
