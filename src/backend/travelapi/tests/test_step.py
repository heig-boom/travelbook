from django.test import TestCase

from travelapi.models import Step, TravelUser, Travel


class StepModelTest(TestCase):
    """Class that tests the step model"""

    def create_step(self,
                    travel,
                    title="L’église Saint Marc, l'emblème de Zagreb",
                    description="Crkva Sv Marka en croate, au cas où vous auriez besoin de demander votre chemin ou "
                                "de vous repérer via la signalisation. Cette église, c’est le symbole de Zagreb, "
                                "avec son toit coloré reconnaissable entre mille. Il n’est pas possible de visiter "
                                "l’intérieur, malheureusement, donc cela ne sera qu’un court passage lors de votre "
                                "visite de la ville haute, le temps d’admirer l’architecture et de prendre quelques "
                                "belles photos. Surtout que les bâtiments autour et la place piétonne sont vraiment "
                                "pittoresques également. On se croit à une autre époque.",
                    start_date="2019-01-01 20:46:57+00:00",
                    end_date="2019-01-02 20:46:57+00:00",
                    street="Trg Sv. Marka",
                    street_num=5,
                    postal_code=10000,
                    city="Zagreb",
                    longitude=45.816368,
                    latitude=15.973488
                    ):
        """Create a step object"""
        return Step.objects.create(travel=travel,
                                   title=title,
                                   description=description,
                                   start_date=start_date,
                                   end_date=end_date,
                                   street=street,
                                   streetNum=street_num,
                                   postal_code=postal_code,
                                   city=city,
                                   longitude=longitude,
                                   latitude=latitude)

    def test_step_creation(self):
        """Create a step object, then test it"""
        travel_user = TravelUser.objects.create(email="test@travelbook.beer", first_name="test", last_name="test",
                                                username="test", password="pass")
        zagreb_travel = Travel.objects.create(title="Travel to Zagreb", banner="/banner/Zagreb.jpg", user=travel_user)

        saint_marc = self.create_step(zagreb_travel)

        self.assertTrue(isinstance(saint_marc, Step))
        self.assertEqual(saint_marc.__str__(), saint_marc.title)
