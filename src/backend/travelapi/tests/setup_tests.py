import tempfile

from PIL import Image
from django.urls import reverse
from rest_framework.test import APITestCase

from travelapi.models import TravelUser, Travel, Step, Subscribe, Like


class SetUpForAllEndpointTests(APITestCase):
    """Parent class to provide a common setUp to subclasses"""

    def setUp(self):
        """Create some users, travels and likes"""
        # Users
        self.amandine = TravelUser.objects.create_user(
            email="amandine@travelbook.beer",
            first_name="test",
            last_name="test",
            username="Amandine",
            password="pass"
        )
        self.charlene = TravelUser.objects.create_user(
            email="charlene@travelbook.beer",
            first_name="test",
            last_name="test",
            username="Charlène",
            password="1234"
        )
        self.jael = TravelUser.objects.create_user(
            email="jael@travelbook.beer",
            first_name="test",
            last_name="test",
            username="Jael24",
            password="pass"
        )
        self.luc = TravelUser.objects.create_user(
            email="luc@travelbook.beer",
            first_name="test",
            last_name="test",
            username="Laykel",
            password="1234"
        )
        self.loris = TravelUser.objects.create_user(
            email="loris@travelbook.beer",
            first_name="test",
            last_name="test",
            username="texx94",
            password="salut"
        )
        self.mateo = TravelUser.objects.create_user(
            email="mateo@travelbook.beer",
            first_name="test",
            last_name="test",
            username="mtutic",
            password="mateo"
        )
        self.traveluser = TravelUser.objects.create_user(
            email="test@travelbook.beer",
            first_name="test",
            last_name="test",
            username="test",
            password="pass"
        )
        self.archie = TravelUser.objects.create_user(
            email="archie@travelbook.beer",
            first_name="test",
            last_name="test",
            username="Archie",
            password="hello"
        )
        self.bob = TravelUser.objects.create_user(
            email="bob@travelbook.beer",
            first_name="test",
            last_name="test",
            username="Bob",
            password="bob"
        )
        self.alice = TravelUser.objects.create_user(
            email="alice@travelbook.beer",
            first_name="test",
            last_name="test",
            username="Alice",
            password="alice"
        )

        self.invalid_traveluser_id = self.amandine.pk + self.charlene.pk + self.jael.pk + self.luc.pk + self.loris.pk \
                                                      + self.mateo.pk + self.traveluser.pk + self.archie.pk \
                                                      + self.bob.pk + self.alice.pk

        # Travels
        self.usa = Travel.objects.create(
            title="Road Trip aux USA",
            user=self.jael
        )
        self.bali = Travel.objects.create(
            title="Semaine à Bali",
            user=self.amandine
        )
        self.yverdon = Travel.objects.create(
            title="Voyage au centre d'Yverdon",
            banner="banners/Yverdon",
            user=self.jael
        )
        self.lausanne = Travel.objects.create(
            title="Le tour de Lausanne en 80 jours",
            banner="banners/Lausanne",
            user=self.alice
        )
        self.travel = Travel.objects.create(
            title="Road Trip aux USA",
            banner="banners/USA",
            user=self.jael
        )

        self.invalid_travel_id = self.usa.pk + self.bali.pk + self.yverdon.pk + self.lausanne.pk + self.travel.pk

        # Steps
        self.new_york = Step.objects.create(
            travel=self.usa,
            title="New York",
            description="Visite de Manhattan, en particulier Central Park et ses écureuils.",
            start_date="2019-05-04 20:46:57+00:00",
            end_date="2019-05-08 20:46:57+00:00",
            street="Central Park West",
            streetNum=0,
            postal_code=10001,
            city="New York",
            longitude=-73.968565,
            latitude=40.779897
        )
        self.grand_canyon = Step.objects.create(
            travel=self.travel,
            title="Grand Canyon",
            description="Le plus grand canyon de la terre, magnifique.",
            start_date="2019-05-08 20:46:57+00:00",
            end_date="2019-05-09 20:46:57+00:00",
            street="Meadview",
            streetNum=0,
            postal_code=86444,
            city="Meadview",
            longitude=-112.1401108,
            latitude=36.0544445)

        self.las_vegas = Step.objects.create(
            travel=self.travel,
            title="Las Vegas",
            description="Nuit à l'hôtel Bellagio. Pas mal, sans plus.",
            start_date="2019-05-09 20:46:57+00:00",
            end_date="2019-05-10 20:46:57+00:00",
            street="Las Vegas Blvd",
            streetNum=3600,
            postal_code=89109,
            city="Las Vegas",
            longitude=-115.176704,
            latitude=36.112625)
        self.ubud = Step.objects.create(
            travel=self.bali,
            title="Ubud",
            description="Visite de la forêt des singes d'Ubud",
            start_date="2018-10-09 20:46:57+00:00",
            end_date="2018-10-12 20:46:57+00:00",
            street="Jl. Monkey Forest",
            streetNum=1,
            postal_code=80571,
            city="Ubud",
            longitude=115.25502898,
            latitude=-8.51795792816
        )
        self.tacos_planet = Step.objects.create(
            travel=self.yverdon,
            title="Tacos Planet",
            description="Tacos Planet est le meilleur restaurant du centre d'Yverdon. Ses plats principaux sont le "
                        "tacos merguez, le tacos cordon bleu et le tacos viande hachée.",
            start_date="2019-12-10 20:46:57+00:00",
            end_date="2019-12-12 20:46:57+00:00",
            street="Rue du Milieu",
            streetNum=42,
            postal_code=1400,
            city="Yverdon-les-Bains",
            longitude=6.637756,
            latitude=46.779205
        )
        self.flon = Step.objects.create(
            travel=self.lausanne,
            title="Flon",
            description="Le génial quartier du Flon comporte de nombreux magasins, dont la Fnac.",
            start_date="2019-12-12 20:46:57+00:00",
            end_date="2019-12-13 20:46:57+00:00",
            street="Rue de Genève 6, 1003 Lausanne",
            streetNum=6,
            postal_code=1003,
            city="Lausanne",
            longitude=6.6291898,
            latitude=46.5218475
        )

        self.invalid_step_id = self.new_york.pk + self.grand_canyon.pk + self.las_vegas.pk + self.ubud.pk \
                                                + self.tacos_planet.pk + self.flon.pk

        # Subscribe
        self.charleneFollowAmandine = Subscribe.objects.create(
            subscribed=self.amandine,
            subscriber=self.charlene
        )
        self.charleneFollowArchie = Subscribe.objects.create(
            subscribed=self.archie,
            subscriber=self.charlene
        )
        self.amandineFollowArchie = Subscribe.objects.create(
            subscribed=self.archie,
            subscriber=self.amandine
        )
        self.bobFollowAlice = Subscribe.objects.create(
            subscribed=self.alice,
            subscriber=self.bob
        )

        # Like
        Like.objects.create(
            user=self.bob,
            travel=self.lausanne
        )

        # Get token for Amandine, Jael, and traveluser
        user = {
            'username': self.amandine.username,
            'first_name': self.amandine.first_name,
            'last_name': self.amandine.last_name,
            'password': 'pass'
        }
        response = self.client.post(reverse('token_obtain_pair'), user, format='json')
        self.amandine_token = response.data['access']

        user = {
            'username': self.jael.username,
            'first_name': self.jael.first_name,
            'last_name': self.jael.last_name,
            'password': 'pass'
        }
        response = self.client.post(reverse('token_obtain_pair'), user, format='json')
        self.jael_token = response.data['access']

        user = {
            'username': self.traveluser.username,
            'first_name': self.traveluser.first_name,
            'last_name': self.traveluser.last_name,
            'password': 'pass'
        }

        response = self.client.post(reverse('token_obtain_pair'), user, format='json')
        self.traveluser_token = response.data['access']

        # Create various data
        self.update_step = {
            'travel': self.yverdon.pk,
            'title': 'Tacos Planet',
            'description': 'Tacos Planet est le 2e meilleur restaurant du centre d\'Yverdon. Il est, selon Loris, '
                           'juste derrière le restaurant Chez Denis.',
            'start_date': '2020-01-01T08:00:00',
            'end_date': '2020-01-01T12:00:00',
            'street': 'Rue du Milieu',
            'streetNum': 42,
            'postal_code': 1400,
            'city': 'Yverdon-les-Bains',
            'longitude': 6.637756,
            'latitude': 46.779205
        }

        self.new_step = {
            'travel': self.travel.pk,
            'title': self.new_york.title,
            'description': self.new_york.description,
            'start_date': self.new_york.start_date,
            'end_date': self.new_york.end_date,
            'street': self.new_york.street,
            'streetNum': self.new_york.streetNum,
            'postal_code': self.new_york.postal_code,
            'city': self.new_york.city,
            'longitude': self.new_york.longitude,
            'latitude': self.new_york.latitude
        }

        # Create image
        image = Image.new('RGB', (100, 100))
        self.tmp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
        image.save(self.tmp_file)
