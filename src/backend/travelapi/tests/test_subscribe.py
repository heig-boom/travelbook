from django.test import TestCase

from travelapi.models import Subscribe, TravelUser


class SubscribeModelTest(TestCase):
    """Class that tests the subscribe model"""

    def create_subscribe(self, subscriber, subscribed):
        """Create a subscribe object"""
        return Subscribe.objects.create(subscriber=subscriber, subscribed=subscribed)

    def test_subscribe_creation(self):
        """Create a subscribe object, then test it"""
        subscriber = TravelUser.objects.create(email="subscriber@travelbook.beer", first_name="test",
                                               last_name="test", username="Subscriber", password="pass")
        subscribed = TravelUser.objects.create(email="subscribed@travelbook.beer", first_name="test",
                                               last_name="test", username="Subscribed", password="pass")

        subscribe = self.create_subscribe(subscriber, subscribed)

        self.assertTrue(isinstance(subscribe, Subscribe))