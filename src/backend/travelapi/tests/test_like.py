from django.test import TestCase

from travelapi.models import Like, TravelUser, Travel


class LikeModelTest(TestCase):
    """Class that tests the like model"""

    def create_like(self, user, travel):
        """Create a like object"""
        return Like.objects.create(user=user, travel=travel)

    def test_like_creation(self):
        """Create a like object, then test it"""
        travel_user = TravelUser.objects.create(email="test@travelbook.beer", first_name="test", last_name="test",
                                                username="test", password="pass")
        zagreb_travel = Travel.objects.create(title="Travel to Zagreb", banner="/banner/Zagreb.jpg", user=travel_user)

        like = self.create_like(travel_user, zagreb_travel)

        self.assertTrue(isinstance(like, Like))
