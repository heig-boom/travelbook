from django.test import Client
from django.urls import reverse
from rest_framework import status

from travelapi.models import TravelUser
from travelapi.serializers.travelusers_ser import TraveluserSerializer
# initialize the APIClient app
from travelapi.tests.setup_tests import SetUpForAllEndpointTests

client = Client()


class GetSingleTraveluserTest(SetUpForAllEndpointTests):
    """Test module for endpoint: GET single traveluser"""

    def test_get_valid_single_traveluser(self):
        """Create some travelusers, and test one valid of them"""
        response = client.get(reverse('traveluser', kwargs={'id': self.jael.pk}))
        traveluser = TravelUser.objects.get(pk=self.jael.pk)
        serializer = TraveluserSerializer(traveluser)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_traveluser(self):
        response = client.get(reverse('traveluser', kwargs={'id': self.invalid_traveluser_id}))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewTraveluserTest(SetUpForAllEndpointTests):
    """Test module for inserting a new traveluser"""

    def test_create_valid_user(self):
        """Tests to POST a new traveluser"""
        with open(self.tmp_file.name, 'rb') as data:
            new_user = {
                'username': 'testuser',
                'first_name': 'test',
                'last_name': 'test',
                'email': 'test@user.ch',
                'password': 'pass'
            }

            response = self.client.post(reverse('register'), new_user, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_user(self):
        """Tests to POST a new invalid traveluser"""
        # Missing infos
        invalid_user = {
            'username': 'testuser',
            'password': 'pass'
        }

        response = self.client.post(reverse('register'), invalid_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeleteTraveluserTest(SetUpForAllEndpointTests):
    """Test module for deleting an existing traveluser"""

    def test_delete_traveluser_without_auth(self):
        """Tests for authorization when deleting an existing traveluser"""
        response = self.client.delete(
            reverse('traveluser', kwargs={'id': self.jael.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_valid_delete_traveluser(self):
        """Tests to DELETE an existing traveluser"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        response = self.client.delete(
            reverse('traveluser', kwargs={'id': self.jael.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_traveluser(self):
        """Tests to DELETE an invalid traveluser"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        response = self.client.delete(
            reverse('traveluser', kwargs={'id': self.invalid_traveluser_id}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class UpdateTraveluserTest(SetUpForAllEndpointTests):
    """Test module for updating an existing traveluser"""

    def test_update_traveluser_without_auth(self):
        """Tests for authorization when updating an existing traveluser"""
        update_jael = {
            'username': 'Jael2404',
            'first_name': 'Jael',
            'last_name': 'Dubey',
            'email': 'jael2404@travelbook.beer',
            'password': 'jael2404'
        }

        response = self.client.patch(
            reverse('traveluser', kwargs={'id': self.jael.pk}),
            update_jael,
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_valid_update_traveluser(self):
        """Tests to UPDATE an existing traveluser"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        update_jael = {
            "username": "Jael",
            "first_name": "Jael",
            "last_name": "Dubey",
            "email": "jael.dubey@travelbook.beer",
            "password": "salut"
        }

        response = self.client.patch(
            reverse('traveluser', kwargs={'id': self.jael.pk}),
            update_jael,
            format='multipart'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_traveluser(self):
        """Tests to UPDATE an invalid traveluser"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        invalid_update = {
            'username': '',
            'email': '',
            'password': ''
        }
        response = self.client.patch(
            reverse('traveluser', kwargs={'id': self.jael.pk}),
            invalid_update,
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
