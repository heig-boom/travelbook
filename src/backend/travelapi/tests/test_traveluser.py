from django.test import TestCase

from travelapi.models import TravelUser


class TravelUserModelTest(TestCase):
    """Class that tests the traveluser model"""

    def create_traveluser(self, email="test@travelbook.beer", first_name="test", last_name="test", username="test",
                          password="pass"):
        """Create a traveluser object"""
        return TravelUser.objects.create(email=email, first_name=first_name, last_name=last_name, username=username,
                                         password=password)

    # TODO Tests has perm(s) methods

    def test_traveluser_creation(self):
        """Create a traveluser object, then test it"""
        traveluser = self.create_traveluser()

        self.assertTrue(isinstance(traveluser, TravelUser))
        self.assertEqual(traveluser.__str__(), traveluser.username)
