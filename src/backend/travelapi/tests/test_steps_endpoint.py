from django.test import Client
from django.urls import reverse
from rest_framework import status

from travelapi.models import Step
# initialize the APIClient app
from travelapi.serializers.steps_ser import StepSerializer
from travelapi.tests.setup_tests import SetUpForAllEndpointTests

client = Client()


class GetAllStepsOfOneTravelTest(SetUpForAllEndpointTests):
    """ Test module for GET all steps of one travel """

    def test_get_all_steps(self):
        """Create some steps, then tests it"""
        # get API response
        response = client.get(
            reverse('steps', kwargs={'id': self.travel.pk}))
        # get data from db
        steps = Step.objects.filter(travel=self.travel)
        serializer = StepSerializer(steps, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_list_of_steps(self):
        """Try to get an invalid list of steps"""
        response = client.get(
            reverse('steps', kwargs={'id': self.invalid_travel_id})
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetSingleStepOfOneTravelTest(SetUpForAllEndpointTests):
    """Test module for GET single step"""

    def test_get_valid_single_step(self):
        """Create a valid step, and tests it"""
        response = client.get(
            reverse('step', kwargs={'id_travel': self.usa.pk, 'id_step': self.new_york.pk}))
        step = Step.objects.get(pk=self.new_york.pk)
        serializer = StepSerializer(step)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_step(self):
        """Tests an invalid step"""
        response = client.get(
            reverse('step', kwargs={'id_travel': self.invalid_travel_id, 'id_step': self.invalid_step_id}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewTravelTest(SetUpForAllEndpointTests):
    """Test module for inserting a new step"""

    def test_create_step_without_auth(self):
        """Tests for authorization when posting a new step"""
        response = self.client.post(reverse('steps', kwargs={'id': self.travel.pk}), self.new_step, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_valid_step(self):
        """Tests to POST a new step"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        response = self.client.post(reverse('steps', kwargs={'id': self.travel.pk}), self.new_step, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_step(self):
        """Tests to POST a new invalid step"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        invalid_travel = {
            'invalid': 'test'
        }

        response = self.client.post(reverse('steps', kwargs={'id': self.travel.pk}), invalid_travel, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateStepTest(SetUpForAllEndpointTests):
    """Test module for updating an existing step"""

    def test_update_step_without_auth(self):
        """Tests for authorization when updating an existing step"""

        response = self.client.put(
            reverse('step', kwargs={'id_travel': self.yverdon.pk, 'id_step': self.tacos_planet.pk}),
            self.update_step,
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_valid_step(self):
        """Tests to UPDATE an existing step"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        response = self.client.put(
            reverse('step', kwargs={'id_travel': self.yverdon.pk, 'id_step': self.tacos_planet.pk}),
            self.update_step,
            format='multipart')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_invalid_step(self):
        """Tests to UPDATE an invalid step"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        # There is no title in this request
        invalid_step = {
            'description': ''
        }

        response = self.client.put(
            reverse('step', kwargs={'id_travel': self.yverdon.pk, 'id_step': self.tacos_planet.pk}),
            invalid_step,
            format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
