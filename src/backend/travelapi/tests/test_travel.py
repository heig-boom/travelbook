from django.test import TestCase

from travelapi.models import Travel, TravelUser


class TravelModelTest(TestCase):
    """Class that tests the travel model"""

    def create_travel(self, user, title="Travel to Zagreb", banner="banners/"):
        """Create a travel object"""
        return Travel.objects.create(title=title, banner=banner, user=user)

    def test_travel_creation(self):
        """Create a travel object, then test it"""
        travel_user = TravelUser.objects.create(email="test@travelbook.beer", first_name="test", last_name="test",
                                                username="test", password="pass")

        zagreb_travel = self.create_travel(travel_user)

        self.assertTrue(isinstance(zagreb_travel, Travel))
        self.assertEqual(zagreb_travel.__str__(), zagreb_travel.title)
