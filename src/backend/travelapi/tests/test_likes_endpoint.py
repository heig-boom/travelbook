from django.urls import reverse
from rest_framework import status

from travelapi.models import Like
from travelapi.serializers.likes_ser import LikeSerializer
from travelapi.tests.setup_tests import SetUpForAllEndpointTests


class GetLikesOfOneTravelTest(SetUpForAllEndpointTests):
    """Unit tests for the Likes GET endpoint"""

    def test_get_all_likes(self):
        """Create some likes and compare the DB to the endpoint result"""
        # get API response
        response = self.client.get(reverse('likes', kwargs={'id': self.bali.pk}))
        # get data from db
        likes = Like.objects.filter(travel=self.bali.pk)
        serializer = LikeSerializer(likes, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CreateNewTravelTest(SetUpForAllEndpointTests):
    """Test module for inserting a new like"""

    def test_create_like_without_auth(self):
        """Tests for authorization when posting a new like"""
        response = self.client.post(
            reverse('likes', kwargs={'id': self.bali.pk}),
            {'user': self.amandine.pk},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_valid_like(self):
        """Tests to POST a new like"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.amandine_token)

        response = self.client.post(
            reverse('likes', kwargs={'id': self.bali.pk}),
            {'user': self.amandine.pk},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_like(self):
        """Tests to POST a new invalid like"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.amandine_token)

        response = self.client.post(
            reverse('likes', kwargs={'id': self.bali.pk}),
            {'travel': self.amandine.pk},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
