from django.test import TestCase

from travelapi.models import Image, TravelUser, Travel, Step


class ImageModelTest(TestCase):
    """Class that tests the image model"""

    def create_image(self, step, travel, url="gallery/stepSaintMarc"):
        """Create an image"""
        return Image.objects.create(step=step, travel=travel, url=url)

    def test_image_creation(self):
        """Create an image object, then test it"""
        travel_user = TravelUser.objects.create(email="test@travelbook.beer", first_name="test", last_name="test",
                                                username="test", password="pass")
        zagreb_travel = Travel.objects.create(title="Travel to Zagreb", banner="/banner/Zagreb.jpg", user=travel_user)

        saint_marc = Step.objects.create(travel=zagreb_travel,
                                         title="L’église Saint Marc, l'emblème de Zagreb",
                                         description="Crkva Sv Marka en croate, au cas où vous auriez besoin de "
                                                     "demander votre chemin ou "
                                                     "de vous repérer via la signalisation. Cette église, c’est le "
                                                     "symbole de Zagreb, "
                                                     "avec son toit coloré reconnaissable entre mille. Il n’est pas "
                                                     "possible de visiter "
                                                     "l’intérieur, malheureusement, donc cela ne sera qu’un court "
                                                     "passage lors de votre "
                                                     "visite de la ville haute, le temps d’admirer l’architecture et "
                                                     "de prendre quelques "
                                                     "belles photos. Surtout que les bâtiments autour et la place "
                                                     "piétonne sont vraiment "
                                                     "pittoresques également. On se croit à une autre époque.",
                                         start_date="2019-01-01 20:46:57+00:00",
                                         end_date="2019-01-02 20:46:57+00:00",
                                         street="Trg Sv. Marka",
                                         streetNum=5,
                                         postal_code=10000,
                                         city="Zagreb",
                                         longitude=45.816368,
                                         latitude=15.973488)

        image = self.create_image(saint_marc, zagreb_travel)

        self.assertTrue(isinstance(image, Image))
        self.assertEqual(image.__str__(), image.url)
