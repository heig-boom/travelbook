from django.test import Client
from django.urls import reverse
from rest_framework import status

from travelapi.models import Travel
from travelapi.serializers.travels_ser import TravelSerializer
# initialize the APIClient app
from travelapi.tests.setup_tests import SetUpForAllEndpointTests

client = Client()


# TODO indicate source ? https://realpython.com/test-driven-development-of-a-django-restful-api/#routes-and-testing-tdd
# TODO Refactor all user to traveluser?


class GetAllTravelsTest(SetUpForAllEndpointTests):
    """Test module for endpoint: GET all travels"""

    def test_get_all_travels(self):
        """Create some travels, then test them"""
        # get API response
        response = client.get(reverse('travels'))
        # get data from db
        travels = Travel.objects.all()
        serializer = TravelSerializer(travels, many=True)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleTravelTest(SetUpForAllEndpointTests):
    """Test module for endpoint: GET single travel"""

    def test_get_valid_single_travel(self):
        """Create some travels, and test one valid of them"""
        response = client.get(reverse('travel', kwargs={'id': self.bali.pk}))
        travel = Travel.objects.get(pk=self.bali.pk)
        serializer = TravelSerializer(travel)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_travel(self):
        """Create some travels, and test one invalid of them"""
        response = client.get(reverse('travel', kwargs={'id': self.invalid_travel_id}))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetTravelsWithRadiusTest(SetUpForAllEndpointTests):

    def test_get_travels_in_radius(self):
        """Create some travels, and test if they are in the radius"""

        # Distance between yverdon and given points = 19.7 km
        # Distance between lausanne and given points = 38.3 km
        response = client.get(reverse('travels') + '?lat=46.815539&long=6.8910259&radius=20.0')
        travels = Travel.objects.filter(pk=self.yverdon.pk)
        serializer = TravelSerializer(travels, many=True)
        self.assertEquals(response.data['results'], serializer.data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_get_travels_out_of_radius(self):
        """Create some travels, and test if they are out of the radius"""

        response = client.get(reverse('travels') + '?lat=46.815539&long=6.8910259&radius=10.0')
        travels = Travel.objects.filter(pk=self.invalid_travel_id)
        serializer = TravelSerializer(travels, many=True)
        self.assertEquals(response.data['results'], serializer.data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_get_travels_invalid_latitude(self):
        """When latitude filter value isn't in [-90,90] interval, it must throw a 422 error"""

        response = client.get(reverse('travels') + '?lat=91.000&long=1.0000&radius=10.0')
        self.assertEquals(response.status_code, status.HTTP_422_UNPROCESSABLE_ENTITY)

    def test_get_travels_invalid_longitude(self):
        """When longitude filter value isn't in [-180,180] interval, it must throw a 422 error"""

        response = client.get(reverse('travels') + '?lat=-10.000&long=-181.00&radius=10.0')
        self.assertEquals(response.status_code, status.HTTP_422_UNPROCESSABLE_ENTITY)


class GetTravelsWithTitleFilterTest(SetUpForAllEndpointTests):

    def test_get_travels_search_by_valid_title(self):
        """Create some travels, and test if their title contains the given parameter"""
        response = client.get(reverse('travels') + '?contains=yverdon')
        travels = Travel.objects.filter(pk=self.yverdon.pk)
        serializer = TravelSerializer(travels, many=True)
        self.assertEquals(response.data['results'], serializer.data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_get_travels_search_by_invalid_title(self):
        """Create some travels, and check that no travel are released when searching an invalid title"""
        response = client.get(reverse('travels') + '?contains=salut')
        travels = Travel.objects.filter(pk=self.invalid_travel_id)
        serializer = TravelSerializer(travels, many=True)
        self.assertEquals(response.data['results'], serializer.data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)


class GetTravelsWithTraveluserIdFilterTest(SetUpForAllEndpointTests):

    def test_get_travels_filter_with_valid_traveluser_id(self):
        """Create some travels, and test if their creator is the given traveluser"""
        response = client.get(reverse('travels') + '?traveluser_id={}'.format(self.traveluser.pk))
        travels = Travel.objects.filter(user_id__exact=self.traveluser.pk)
        serializer = TravelSerializer(travels, many=True)
        self.assertEquals(response.data['results'], serializer.data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_get_travels_filter_with_invalid_traveluser_id(self):
        """Create some travels, and check that no travel are released when filtering with an invalid user"""
        response = client.get(reverse('travels') + '?traveluser_id={}'.format(self.invalid_traveluser_id))
        travels = Travel.objects.filter(pk=self.invalid_travel_id)
        serializer = TravelSerializer(travels, many=True)
        self.assertEquals(response.data['results'], serializer.data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)


class GetTravelsWithSubscribeFilterTest(SetUpForAllEndpointTests):

    def test_get_travels_with_subscribe_filter(self):
        """Create some travels, and test if it is possible to filter them by subscribe relationship"""
        response = client.get(reverse('travels') + '?friends_filter={}'.format(self.bob.pk))
        travels = Travel.objects.filter(pk=self.lausanne.pk)
        serializer = TravelSerializer(travels, many=True)
        self.assertEquals(response.data['results'], serializer.data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)


class GetTravelsWithLikesFilterTest(SetUpForAllEndpointTests):

    def test_get_travels_with_likes_filter(self):
        """Create some travels, and test if it is possible to filter them by like relationship"""
        response = client.get(reverse('travels') + '?likes_filter={}'.format(self.bob.pk))
        travels = Travel.objects.filter(pk=self.lausanne.pk)
        serializer = TravelSerializer(travels, many=True)
        self.assertEquals(response.data['results'], serializer.data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)


class CreateNewTravelTest(SetUpForAllEndpointTests):
    """Test module for inserting a new travel"""

    def test_create_travel_without_auth(self):
        """Tests for authorization when posting a new travel"""

        response = self.client.post(
            reverse('travels'),
            {'title': self.usa.title, 'user': self.usa.user.pk},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_valid_travel(self):
        """Tests to POST a new travel"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        with open(self.tmp_file.name, 'rb') as data:
            new_travel = {
                'banner': data,
                'title': self.usa.title,
                'user': self.usa.user.pk
            }

            response = self.client.post(reverse('travels'), new_travel, format='multipart')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_travel(self):
        """Tests to POST a new invalid travel"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        # There is no title in this request
        invalid_travel = {
            'user': self.usa.user.pk
        }

        response = self.client.post(reverse('travels'), invalid_travel, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateTravelTest(SetUpForAllEndpointTests):
    """Test module for updating an existing travel"""

    def test_update_travel_without_auth(self):
        """Tests for authorization when updating an existing travel"""

        response = self.client.put(
            reverse('travel', kwargs={'id': self.yverdon.pk}),
            {'title': self.yverdon.title, 'user': self.yverdon.user.pk},
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_valid_travel(self):
        """Tests to UPDATE an existing travel"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        with open(self.tmp_file.name, 'rb') as data:
            put_travel = {
                'banner': data,
                'title': 'Yverdon-les-Bains',
                'user': self.yverdon.user.pk
            }

            response = self.client.put(reverse('travel', kwargs={'id': self.yverdon.pk}),
                                       put_travel,
                                       format='multipart')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_invalid_travel(self):
        """Tests to UPDATE an invalid travel"""
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.jael_token)

        # There is no title in this request
        invalid_travel = {
            'user': self.yverdon.user.pk
        }

        response = self.client.put(reverse('travel', kwargs={'id': self.yverdon.pk}), invalid_travel, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
