from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView

from travelapi.models import Like, Travel
from travelapi.serializers.likes_ser import LikeSerializer


class Likes(APIView):
    """Get and Post likes from a user to a travel"""
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get(self, request, id):
        # Raise 404 error if traveluser with given id doesn't exist
        get_object_or_404(Travel, id=id)

        likes = Like.objects.filter(travel=id)
        serializer = LikeSerializer(likes, many=True)
        return Response(serializer.data)

    def post(self, request, id):
        """Create a Like"""
        # Make the immutable dict mutable
        req = request.data.copy()
        # Set the id of the travel to the id of the endpoint
        req['travel'] = id
        serializer = LikeSerializer(data=req)

        # TODO Maybe do something about duplication
        if serializer.is_valid(raise_exception=True):
            # Check that the user is who he pretends to be
            if int(request.data['user']) != request.user.id:
                return Response({"detail": "You are not logged in as that user."}, status=status.HTTP_401_UNAUTHORIZED)

            serializer.save()
            return Response({
                "detail": "success",
                "id": "{}".format(serializer.data['id'])
            }, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
