from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from travelapi.models import Subscribe, TravelUser
from travelapi.serializers.subscribe_ser import SubscribeSerializer


class AllSubscribed(APIView):
    """List all subscribed traveluser of a given traveluser"""
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get(self, request, id):
        # Raise 404 error if traveluser with given id doesn't exist
        get_object_or_404(TravelUser, id=id)

        subscribed = Subscribe.objects.filter(subscriber=id)
        serializer = SubscribeSerializer(subscribed, many=True)
        return Response(serializer.data)

    def post(self, request):
        """Subscribe to a user"""
        serializer = SubscribeSerializer(data=request.data)

        # TODO Maybe do something about duplication
        if serializer.is_valid(raise_exception=True):
            # Check that the user is who he pretends to be
            if int(request.data['subscriber']) != request.user.id:
                return Response({"detail": "You are not logged in as that user."}, status=status.HTTP_401_UNAUTHORIZED)

            serializer.save()
            return Response({
                "detail": "success",
                "id": "{}".format(serializer.data['id'])
            }, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AllSubscribers(APIView):
    """List all subscribers traveluser of a given traveluser"""
    def get(self, request, id):
        # Raise 404 error if traveluser with given id doesn't exist
        get_object_or_404(TravelUser, id=id)

        subscriber = Subscribe.objects.filter(subscribed=id)
        serializer = SubscribeSerializer(subscriber, many=True)
        return Response(serializer.data)
