from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from travelapi.models import Step, Travel
from travelapi.pagination import PaginationHandlerMixin, BasicPagination
from travelapi.serializers.steps_ser import StepSerializer
from travelapi.serializers.images_ser import ImageSerializer


class AllSteps(APIView, PaginationHandlerMixin):
    """List all steps of a travel in an endpoint"""
    pagination_class = BasicPagination
    serializer_class = StepSerializer

    def get(self, request, format=None, *args, **kwargs):
        get_object_or_404(Travel, id=kwargs.get('id', 0))

        steps = Step.objects.filter(travel=kwargs.get('id', 0))
        page = self.paginate_queryset(steps)
        if page is not None:
            serializer = self.get_paginated_response(self.serializer_class(page, many=True).data)
        else:
            serializer = self.serializer_class(steps, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, id):
        """Create a step from the above data"""
        req = request.data.copy()
        req['travel'] = id

        serializer = StepSerializer(data=req)

        travel = Travel.objects.get(pk=id)

        if serializer.is_valid(raise_exception=True):
            # Check that the user is who he pretends to be
            if request.user.id != travel.user.pk:
                return Response({"detail": "You are not logged in as that user."}, status=status.HTTP_401_UNAUTHORIZED)

            serializer.save()

            # Create images
            if 'images' in request.data:
                images = dict(request.data.lists())['images']
                for image in images:
                    field = {'travel': id, 'step': serializer.data['id'], 'url': image}
                    img_serializer = ImageSerializer(data=field)

                    if img_serializer.is_valid():
                        img_serializer.save()

            return Response({
                "detail": "success",
                "id": "{}".format(serializer.data['id'])
            }, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OneStep(APIView):
    """Show one step of a travel in an endpoint"""
    serializer_class = StepSerializer

    def get(self, request, format=None, *args, **kwargs):
        step = get_object_or_404(Step, id=kwargs.get('id_step', 0), travel=kwargs.get('id_travel', 0))
        serializer = self.serializer_class(step, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, format=None, *args, **kwargs):
        """Update a step"""
        step = get_object_or_404(Step, id=kwargs.get('id_step', 0), travel=kwargs.get('id_travel', 0))

        # Check that the user is who he pretends to be
        if request.user.id != step.travel.user.pk:
            return Response({"detail": "You are not logged in as that user."}, status=status.HTTP_401_UNAUTHORIZED)

        serializer = StepSerializer(step, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

