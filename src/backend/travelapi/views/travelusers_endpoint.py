from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from travelapi.models import TravelUser
from travelapi.serializers.travelusers_ser import TraveluserSerializer


class Travelusers(APIView):
    def post(self, request):
        """Create a new traveluser"""
        # Create a traveluser from the above data
        serializer = TraveluserSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response({
                "detail": "success",
                "id": "{}".format(serializer.data['id'])
            }, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OneTraveluser(APIView):
    def get(self, request, format=None, *args, **kwargs):
        """Return the traveluser requested"""
        traveluser = get_object_or_404(TravelUser, id=kwargs.get('id', 0))
        serializer = TraveluserSerializer(traveluser, many=False)
        return Response(serializer.data)

    def delete(self, request, id, format=None):
        """Delete a traveluser"""
        traveluser = get_object_or_404(TravelUser, id=id)

        # Check that the user is who he pretends to be
        if request.user.id != traveluser.pk:
            return Response({"detail": "You are not logged in as that user."}, status=status.HTTP_401_UNAUTHORIZED)

        traveluser.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def patch(self, request, id, format=None):
        """Update a traveluser"""
        traveluser = get_object_or_404(TravelUser, id=id)

        # Check that the user is who he pretends to be
        if request.user.id != traveluser.pk:
            return Response({"detail": "You are not logged in as that user."}, status=status.HTTP_401_UNAUTHORIZED)

        serializer = TraveluserSerializer(traveluser, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

