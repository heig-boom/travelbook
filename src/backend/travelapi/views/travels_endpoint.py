import math

from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView

from travelapi.models import Travel, Step
from travelapi.pagination import PaginationHandlerMixin, BasicPagination
from travelapi.serializers.travels_ser import TravelSerializer


class AllTravels(APIView, PaginationHandlerMixin):
    """Get and Post travels"""
    pagination_class = BasicPagination
    serializer_class = TravelSerializer

    permission_classes = [IsAuthenticatedOrReadOnly]

    def degrees_to_radians(self, degrees):
        """Convert degrees to radians"""
        return degrees * math.pi / 180

    def distance_in_km_between_earth_coordinates(self, lat1, lon1, lat2, lon2):
        """Compute the distance in km between two gps coordinates"""
        earth_radius_km = 6371

        d_lat = self.degrees_to_radians(lat2 - lat1)
        d_lon = self.degrees_to_radians(lon2 - lon1)

        lat1 = self.degrees_to_radians(lat1)
        lat2 = self.degrees_to_radians(lat2)

        a = math.sin(d_lat / 2) * math.sin(d_lat / 2) + math.sin(d_lon / 2) * math.sin(d_lon / 2) * math.cos(lat1) * \
            math.cos(lat2)

        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

        return earth_radius_km * c

    def get(self, request, format=None, *args, **kwargs):
        """Showing some travels in terms of given parameters"""
        latitude = request.GET.get('lat')
        longitude = request.GET.get('long')
        radius = request.GET.get('radius')  # radius in km

        title = request.GET.get('contains')

        traveluser_id = request.GET.get('traveluser_id')

        traveluser_id_for_friends_filter = request.GET.get('friends_filter')

        traveluser_id_for_likes_filter = request.GET.get('likes_filter')

        travels = Travel.objects.all()

        if latitude is not None and longitude is not None and radius is not None:
            travels = self.get_with_gps_filter(travels, latitude, longitude, radius)
            if isinstance(travels, Response):
                return travels
        if title is not None:
            travels = self.get_with_search_by_title(travels, title)
        if traveluser_id is not None:
            travels = self.get_with_user_id_filter(travels, traveluser_id)
        if traveluser_id_for_friends_filter is not None:
            travels = self.get_with_friends_filter(travels, traveluser_id_for_friends_filter)
        if traveluser_id_for_likes_filter is not None:
            travels = self.get_with_likes_filter(travels, traveluser_id_for_likes_filter)

        return self.return_response(travels)

    def get_with_gps_filter(self, travels, lat, long, rad):
        """Show travels which are in the given radius"""
        latitude = float(lat)
        longitude = float(long)
        radius = float(rad)

        if latitude < -90 or latitude > 90:
            return Response({"detail": "Latitude {} isn't in the interval [-90,90].".format(latitude)},
                            status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        if longitude < -180 or longitude > 180:
            return Response({"detail": "Longitude {} isn't in the interval [-180,180].".format(longitude)},
                            status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        to_be_deleted = []

        for travel in travels:
            step = Step.objects.filter(travel=travel).earliest('start_date')
            lat_travel = step.latitude
            long_travel = step.longitude

            distance = self.distance_in_km_between_earth_coordinates(latitude, longitude, lat_travel, long_travel)

            if distance > radius:
                to_be_deleted.append(travel.pk)

        return Travel.objects.exclude(pk__in=to_be_deleted)

    def get_with_user_id_filter(self, travels, traveluser_id):
        """Show travels which are created by a given traveluser"""
        return travels.filter(user_id__exact=traveluser_id)

    def get_with_search_by_title(self, travels, title):
        """Show travels which contains title parameter in their title"""
        return travels.filter(title__icontains=title)

    def get_with_friends_filter(self, travels, traveluser_id):
        """Show travels which are created by a subscriber"""
        return travels.filter(user__userSubscribed__subscriber_id=traveluser_id)

    def get_with_likes_filter(self, travels, traveluser_id):
        """Show travels which are liked by a traveluser"""
        return travels.filter(like__user_id=traveluser_id)

    def return_response(self, queryset):
        """Allow to refactor the returning response value"""
        if isinstance(queryset, Response):
            return queryset
        else:
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_paginated_response(self.serializer_class(page, many=True).data)
            else:
                serializer = self.serializer_class(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        # Create a travel from the above data
        serializer = TravelSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            # Check that the user is who he pretends to be
            if int(request.data['user']) != request.user.id:
                return Response({"detail": "You are not logged in as that user."}, status=status.HTTP_401_UNAUTHORIZED)

            serializer.save()
            return Response({
                "detail": "success",
                "id": "{}".format(serializer.data['id'])
            }, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OneTravel(APIView):
    """Show one travel in an endpoint"""
    serializer_class = TravelSerializer

    def get(self, request, format=None, *args, **kwargs):
        travel = get_object_or_404(Travel, id=kwargs.get('id', 0))
        serializer = self.serializer_class(travel, many=False)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, format=None):
        """Update a travel"""
        travel = get_object_or_404(Travel, id=id)

        # Check that the user is who he pretends to be
        if request.user.id != travel.user.pk:
            return Response({"detail": "You are not logged in as that user."}, status=status.HTTP_401_UNAUTHORIZED)

        serializer = TravelSerializer(travel, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
