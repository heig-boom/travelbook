from rest_framework.pagination import PageNumberPagination


class BasicPagination(PageNumberPagination):
    """Allow to parametrize the pagination of one or more endpoints"""
    page_size_query_param = 'limit'
    page_size = 20


class PaginationHandlerMixin(object):
    """Class which is used by all the paginated endpoints"""
    @property
    def paginator(self):
        if not hasattr(self, '_paginator'):
            if self.pagination_class is None:
                self._paginator = None
            else:
                self._paginator = self.pagination_class()
        else:
            pass
        return self._paginator

    def paginate_queryset(self, queryset):

        if self.paginator is None:
            return None
        return self.paginator.paginate_queryset(queryset, self.request, view=self)

    def get_paginated_response(self, data):
        assert self.paginator is not None
        return self.paginator.get_paginated_response(data)
