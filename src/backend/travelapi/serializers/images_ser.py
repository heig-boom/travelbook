from rest_framework import serializers
from travelapi.models import Image


class ImageSerializer(serializers.ModelSerializer):
    """Class that allows to serialize the images"""

    def get_url(self, image):
        return '/media/' + image.url

    class Meta:
        """Class to choose which fields will be serialized"""
        model = Image
        fields = ('url', 'step', 'travel')
