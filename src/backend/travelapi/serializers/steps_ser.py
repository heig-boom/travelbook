from rest_framework import serializers
from travelapi.models import Step, Image


class StepSerializer(serializers.ModelSerializer):
    """Class that allows to serialize the steps"""
    images = serializers.SerializerMethodField()

    def get_images(self, step):
        # Return all urls of images linked to that step
        return ['/media/'+str(image.url) for image in Image.objects.filter(step=step)]

    def create(self, validated_data):
        """Create a new Step"""
        return Step.objects.create(**validated_data)

    class Meta:
        """Class that choose which step's fields will be serialize"""
        model = Step
        fields = '__all__'
