from rest_framework import serializers

from travelapi.models import Like


class LikeSerializer(serializers.ModelSerializer):
    """Class that allows to serialize the likes"""
    class Meta:
        """Class to choose which fields will be serialized"""
        model = Like
        fields = '__all__'
