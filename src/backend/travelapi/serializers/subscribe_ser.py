from rest_framework import serializers

from travelapi.models import Subscribe


class SubscribeSerializer(serializers.ModelSerializer):
    """Class that allows to serialize the subscribes"""
    class Meta:
        """Class to choose which fields will be serialized"""
        model = Subscribe
        fields = '__all__'
