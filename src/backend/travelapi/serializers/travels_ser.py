from rest_framework import serializers
from travelapi.models import Travel, Step


class TravelSerializer(serializers.ModelSerializer):
    """Class that allows to serialize the travels"""
    start_date = serializers.SerializerMethodField()
    latitude = serializers.SerializerMethodField()
    longitude = serializers.SerializerMethodField()

    def get_banner(self, travel):
        return '/media/' + travel.banner

    def get_start_date(self, travel):
        step = Step.objects.filter(travel=travel).first()
        if step is None:
            return None
        return step.start_date

    def get_latitude(self, travel):
        step = Step.objects.filter(travel=travel).first()
        if step is None:
            return None
        return step.latitude

    def get_longitude(self, travel):
        step = Step.objects.filter(travel=travel).first()
        if step is None:
            return None
        return step.longitude

    def create(self, validated_data):
        """Create a new Travel"""
        return Travel.objects.create(**validated_data)

    class Meta:
        """Class that defines which fields will be serialized"""
        model = Travel
        fields = '__all__'
