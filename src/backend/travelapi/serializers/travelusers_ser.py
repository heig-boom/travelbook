from rest_framework import serializers
from travelapi.models import TravelUser


class TraveluserSerializer(serializers.ModelSerializer):
    """Class that allows to serialize the travelusers"""

    def create(self, validated_data):
        """Create a new Traveluser"""
        user = TravelUser.objects.create(
            username=validated_data['username'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            email=validated_data['email']
        )
        user.set_password(validated_data['password'])
        user.save()

        return user

    class Meta:
        """Class that defines which fields will be serialized"""
        model = TravelUser
        fields = ('id', 'email', 'first_name', 'last_name', 'username', 'password', 'is_active', 'is_admin')
