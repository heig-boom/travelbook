# Generated by Django 2.2.6 on 2020-01-05 17:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('travelapi', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='url',
            field=models.ImageField(upload_to='gallery/%Y/%m/%d/'),
        ),
        migrations.AlterField(
            model_name='travel',
            name='banner',
            field=models.ImageField(null=True, upload_to='banners/%Y/%m/%d/'),
        ),
    ]
