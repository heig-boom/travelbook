from django.db import models

from travelapi.models import TravelUser


class Travel(models.Model):
    """Class that represents a travel"""
    title = models.CharField(max_length=200)
    banner = models.ImageField(upload_to="banners/%Y/%m/%d/", null=True)
    user = models.ForeignKey(TravelUser, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Travel"

    def __str__(self):
        """Return the title of the travel"""
        return self.title
