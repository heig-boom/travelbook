from django.db import models
from django.utils import timezone

from travelapi.models import Travel


class Step(models.Model):
    """Class that represents a step of a travel"""
    travel = models.ForeignKey(Travel, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=10000)
    start_date = models.DateTimeField(default=timezone.now, verbose_name="Start date")
    end_date = models.DateTimeField(verbose_name="End date")
    street = models.CharField(max_length=200)
    streetNum = models.IntegerField()
    postal_code = models.CharField(max_length=10)
    city = models.CharField(max_length=100)
    longitude = models.FloatField(null=True)
    latitude = models.FloatField(null=True)

    class Meta:
        verbose_name = "Step"

    def __str__(self):
        """Return the title of the travel's step"""
        return self.title
