from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


class TravelUserManager(BaseUserManager):
    """Class that allows to manage the traveluser"""
    def create_user(self, email, first_name, last_name, username, password=None):
        """Creates and saves a user with the given email, names, username and password."""
        if not email:
            raise ValueError('Users must have an email address')

        if not username:
            raise ValueError('Users must have a username')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        """Creates and saves a superuser with the given email, names, username and password."""
        user = self.create_user(
            email,
            first_name=username,
            last_name=username,
            username=username,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class TravelUser(AbstractBaseUser):
    """Class that represents a traveluser"""
    email = models.EmailField(verbose_name='email address', max_length=255, unique=True)
    first_name = models.CharField(max_length=200, default='John')
    last_name = models.CharField(max_length=200, default='Doe')
    username = models.CharField(max_length=200, unique=True)
    profile_picture = models.ImageField(upload_to="profile_pics/%Y/%m/%d/", null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = TravelUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'password']

    def __str__(self):
        """Return the username of the traveluser"""
        return self.username

    # TODO Implement this method
    def has_perm(self, perm, obj=None):
        """Does the user have a specific permission?"""
        # Simplest possible answer: Yes, always
        return True

    # TODO Implement this method
    def has_module_perms(self, app_label):
        """Does the user have permissions to view the app `app_label`?"""
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        """Is the user a member of staff?"""
        # Simplest possible answer: All admins are staff
        return self.is_admin
