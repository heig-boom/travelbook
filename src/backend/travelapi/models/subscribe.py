from django.db import models

from travelapi.models import TravelUser


class Subscribe(models.Model):
    """Class that represents a subscribe link (when a traveluser follows another traveluser)"""
    subscriber = models.ForeignKey(TravelUser, on_delete=models.CASCADE, related_name='subscriberToUser')
    subscribed = models.ForeignKey(TravelUser, on_delete=models.CASCADE, related_name='userSubscribed')

    class Meta:
        verbose_name = "Subscribe"
