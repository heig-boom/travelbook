from django.db import models

from travelapi.models import Step, Travel


class Image(models.Model):
    """Class that represents an image"""
    url = models.ImageField(upload_to="gallery/%Y/%m/%d/")
    step = models.ForeignKey(Step, on_delete=models.CASCADE)
    travel = models.ForeignKey(Travel, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Image"

    def __str__(self):
        """Return the url of the image"""
        return self.url
