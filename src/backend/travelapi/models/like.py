from django.db import models

from travelapi.models import TravelUser, Travel


class Like(models.Model):
    """Class that represents a like (when a traveluser likes a travel)"""
    user = models.ForeignKey(TravelUser, on_delete=models.CASCADE)
    travel = models.ForeignKey(Travel, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Like"
