# PDG - TravelBook

## Description

TravelBook is a travel log web app with social aspects. You can add your holiday destinations to your profile along with pictures and personal descriptions. Your destinations will then appear on a map of the world, along with your friends' destinations.

The main interest in the application is to be able to visualize your destinations on a map of the world, and to add pictures and description of your travels. The social aspect is secondary, but adds very interesting features, like commenting on friends' logs.
