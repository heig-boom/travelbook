# Project idea

## Description

TravelBook will be a social network which allows to connect people around a unique subject : travels. The main feature of our application is a world map where you can see all the trips of your friends. It will be possible to see the details of each trip, like which cities were exactly visited, or which hotels were chosen. Users will be able to write comments or like the publications.

