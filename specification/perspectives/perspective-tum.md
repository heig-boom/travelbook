# Project idea

## Description

The aim of the project is to create a social network to share everyone's experiences and travels. TravelBook is a web application represented by a world map where the user can share pictures of places he has visited alone or with his friends.

There is also a news feed that shows the different pictures/activities of the user's friends.
