# Project idea

##### How I see the project

For me, the project will be set like a new social network oriented on travel experiences. Every user will be able to make a story of his trip with some descriptions, images and maybe more.

In a second step, it could be nice to create a "friends system" to allow only a few person to see our trips and also to find easily trips of our friends.

##### What is the final product

TravelBook will be a web application that show us a map of the world and on it some pins representing different trips. Maybe the application will show a flow of new informations and friends's activity.