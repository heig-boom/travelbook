# Calendar

## Week 1 (December 9th) :

### Backend

* ~~Think about authentication and authorization and maybe start to implement them~~
* ~~Resolve 404 errors for some endpoints (when returning empty lists)~~

### Frontend

* ~~Think about authentication and authorization~~
* ~~Add the vuejs router~~
* ~~Start to implement the page to read an existing step (only with one step)~~
* ~~Add the username and profile pic in the header (hard coded for now)~~
* ~~Coordinate with DevOps to resolve *sub-paths* issues~~

### DevOps

* ~~Resolve Let's Encrypt rate limit issue~~
* ~~Resolve *sub-paths* issues with reverse proxy~~


## Week 2 (December 16th) :

### Backend

* ~~Implement authentication and authorization~~

### Frontend

* ~~Finish implementing the page to read an existing step (only with on step) --> Week 3~~
* ~~Start implementing authentication client side~~
* ~~Start to implement the profile page~~

### DevOps

* ~~Set up *prod* stage~~


## Week 3 (December 23rd) :

### Backend

* ~~Secure private endpoints with authorization --> Week 4~~
* ~~Implement all GET endpoints~~
* ~~Start to implement POST endpoints~~

### Frontend

* ~~Finish implementing the page to read an existing step (only with on step) <-- Week 2~~
* Finish with authentication client side (manage JWT tokens) --> Week 4
* Finish implementing the profile page --> Week 4
* Implement the page to create a new trip --> Week 4
* Unit tests --> Week 4

### DevOps

* ~~Clean up, documentation~~


## Week 4 (December 30th) :

### Backend

* ~~Secure private endpoints with authorization <-- Week 3~~
* Finish implementing POST endpoints
* Implement PUT and DELETE endpoints

### Frontend

* Finish with authentication client side (manage JWT tokens) <-- Week 3
* Finish implementing the profile page <-- Week 3
* Implement the page to create a new trip <-- Week 3
* Finish replacing placeholders (remove all hard coded stuff)
* Implement the map...
* Unit tests
