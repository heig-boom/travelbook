# Infrastructure

## Specific GitLab Runner server

This is the server on which the jobs of our pipeline are run.

- It is a private DigitalOcean droplet running `docker` and `gitlab-runner`.
- https://about.gitlab.com/blog/2016/04/19/how-to-set-up-gitlab-runner-on-digitalocean/
- Configuration in `/etc/gitlab-runner/config.toml`:

```
concurrent = 2
check_interval = 0
log_level = "warning"

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Travelbook project runner for CI/CD"
  url = "https://gitlab.com/ci"
  token = "QzRpDoJ6U_3P8T_4sEQ3"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "docker"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

- The runner executes the different stages set in `.gitlab-ci.yml`.

## Staging server

This is the server that runs our application in test phase.

- Folder used for deployment: '/travelbook'.
- Minimal security.
- A firewall is up, stopping anything coming in except on ports 22, 80 and 443.
- No backup of this server is setup.
- (Server location: Frankfurt.)

### Deploy app on test server

- Dockerfiles in root of `backend` and `frontend`.
- Let `.gitlab-ci.yml` run `docker/staging/deploy-test.sh`.
- Write env vars file for server config.
- Update repo on remote server and rerun the containers.

## Production server

- User used for deployment: 'webmaster'.
- Folder used for deployment: '/travelbook'.
- Maximal security.
- A firewall is up, stopping anything coming in except on ports 22, 80 and 443.
- Backups for this server are setup weekly.
- (Server location: London.)

## SSH configuration for deployment

In order to deploy to both our servers, the following steps were taken to be able to use ssh.

- Generate SSH key pair on the server. (`ssh-keygen -t rsa`)
- Add public key to authorized hosts. (`cat .ssh/id_rsa.pub >> .ssh/authorized_keys`)
- Add private key to GitLab env vars, so that the runner can deploy to server. (`cat .ssh/id_rsa`)
