# Final demo scenario

A new user discovers the web application _TravelBook_ through a friend and decides to check it out by submitting his Christmas holiday.

1. The user creates an account.
2. The user logs in with his account.
3. The user looks for his friend who gave him his account name.
4. The user follows his friend and checks out his travels.
    5. He selects one and reads the description.
    6. He looks at the pictures.
    7. He likes the travel with the heart button.
8. The user goes to his liked travels and sees his friend's here.
9. The user creates a new travel and adds the steps he took in his Christmas holiday.
10. The user looks at the map and uses the filters to see his travel.
11. The user uses the filters to see his and his friends' travels.
12. The user uses the filters to see every user's travels.
