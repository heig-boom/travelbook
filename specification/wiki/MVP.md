# Minimum Viable Product

## Description

TravelBook is a social web application for people who want to keep track of their holiday destinations, illustrate them with their favourite pictures and share them with their friends and with the world.

## Feature list

In the following list, it is important to differentiate a travel from a destination. A travel may or may not contain multiple destinations. A destination is like a step in a trip, even though a travel can only contain a single destination.

The user will be able to:

- Look at and interact with a representation of the world (map)
- See other users' destinations on the map
- Use filters to either:
    - See every user's destinations
    - See his and his friends' destinations
    - See only his destinations
- See a list of travels and select one to either:
    - See its constituent destinations or
    - See the description of its unique destination, along with the pictures uploaded
- Add a travel to their list of favourite travels
- Create a new travel and add destinations using their addresses
- Describe his destinations textually
- Illustrate his destinations with pictures
