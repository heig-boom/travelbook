# Architecture choices

## Abstract

We're exposing a REST API using a _Django (2.2.6)_ backend with a _PostgreSQL_ database (manipulated through the Django ORM).

On the frontend, _Vue.js_ builds the interfaces.

## Backend framework

Based on our previous experiences, we resolved to either use Node.js and Express.js on the backend or Django.

Django interested us the most in the end. We will configure it to expose a REST API for our frontend to use. This involves not using the "template" part of its MVT pattern.

## Database

We considered both traditional SQL DBMS like MySQL or PostgreSQL and NoSQL document based DBMS like MongoDB.

Using the backend framework we chose, the only advantage given by MongoDB was its great horizontal scalability. Since it had greater compatibility with SQL DBMS, we chose PostgreSQL.

## Frontend framework

We were advised Vue.js and it seemed very interesting indeed. Its powerful and configurable nature sold it to us.

## Web server

We decided to use web servers that are known to pair well with Django. The first one is a lightweight python server usable for testing and the second is more robust for production.

- Test environment: wsgi
- Production environment: Apache httpd with mod_wsgi
