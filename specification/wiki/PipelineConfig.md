# Pipeline config

This file contains details about the different stages of this project's pipeline, and about the different servers' configuration.

## The pipeline

### Diagram

![Pipeline diagram](PipelineDiagram.png)

### Test job

Trigger: on every commit!

This creates an instance of each of our test images on the GitLab Runner. We run the unit tests for the backend and for the frontend on two separate images.

### Staging job

Trigger: on commit to master (in essence, on every merge).

This deploys the new version of the app to our staging environment, online.

### Production job

Trigger: on commit to master, manual.

When we decide so, we manually activate this job to deploy the app to the production environment. It functions just like the staging job, but doesn't turn off the containers before updating them. This results in very little downtime.

### Landing page job

Trigger: on commit to master, if changes were made to the landing page.

Updates the landing page on GitLab Pages if changes were made. It consists simply of copying the static sources to the public folder of the repo.

---

## Development environment

This is the configuration used for local development. It allows replicating the configuration of our online servers as closely as possible, reverse proxy included.

- Docker-compose setup in `docker/development`.
- All settings are publicly available in the `docker/development/env_file_dev` file.
- Unimportant **secret key** in Django (default one): not used in test server and production.

## Staging environment

This is the test server. It is updated on every merge to the master branch and allows us to test a release as much as we want before deploying it to production.

- Docker-compose setup in `docker/staging`.
- All settings are safely stored in GitLab environment variables and written to server on deploy.
- The database is flushed and rebuilt on every release, with dummy data generated programmatically.

## Production environment

This is the final environment for a version of the app. The configuration of this environment is quite different to the others, since it is supposed to be usable by our users.

- Docker-compose setup in `docker/production`.
- All settings are safely stored in GitLab environment variables and written to server on deploy.
- Most of the logs are disabled and DEBUG mode is off.
- The database is persistent and no dummy test data is generated on this environment.
