# REST endpoints

This document describes the design choices made for Travelbook's REST API. We tried applying best practices wherever we could.

We decided to add a version in our API URL, so that clients (starting with our frontend) don't have to immediately change all their requests when we decide to change the API.

## API v1

### Main URL

Clients can interrogate the API using the following URL.

```
{main site}/api/v1/{resource}
```

- `{main site}` is the URL pointing to the actual site's server. In principle for production use, it should be `https://travelbook.beer`.

- `{resource}` should be replaced by one of the resources of the following sections.

### GET endpoints

These endpoints allow the retrieval of existing resources.

```
travels --> paginated, parameters: userId, lat/long + radius, filters, title (search)
travels/{id}
travels/{id}/likes
travels/{id}/steps --> paginated
travels/{id}/steps/{id}

travelusers/{id} - populate profile page
travelusers/{id}/subscribed
travelusers/{id}/subscribers
```

### POST endpoints

These endpoints allow the creation of resources.

```
travels
travels/{id}/steps
travels/{id}/likes - like the travel

travelusers - registration
travelusers/subscription - subscribe to user

session - log in
```

### DELETE endpoints

These endpoints allow the removal of existing resources.

```
session - log out
travelusers/{id} - remove user
```

### PUT endpoints

These endpoints allow the modification of existing resources.

```
travelusers/{id}
travels/{id}
travels/{id}/steps/{id}
```

### PATCH endpoints

These endpoints allow partial update of existing resources.

```
travelusers/{id}
```
